# Explication

Ce dépot à pour but de rassembler le travail de tous ce qui cherche ou on cherchés des stages TN09 ou TN10 
en informatique dans un environnement de travail si ce n'est de logiciel libre, au moins de développement 
open source. 

Ce dépot git viens du constat qu'au moment d'effectuer une recherche de stage, les seules
offres que nous fournis l'UTC sont des offres traditionnelles dans des entreprises aux moeurs plus ou moins discutables. 
Les etudiants cherchant donc des stages différents on fort à faire simplement pour trouver les entreprises.

# Contribution et Organisation du dépot

pour contribuer il suffit d'ajouter une nouvelle fiche obéissant au template [suivant](./template_entreprise.md) et de la placer dans la section appropriée.
Merci de faire bien attention au template, le script est loin d'être robuste actuellement.
 
 Le dépot est organisé selon l'arborescence suivante : 
 - entreprise
    - étranger
     - europe
     - autre
    - france
     - département
 - site internet
    
Si vous disposez ensuite d'un pc sous linux, vous pouvez exécuter ce [script](./output/setup.sh) pour mettre à jour la liste générale des entreprises qu'on peut trouver pour l'instant [ici](./output/liste.md)

## amélioration futures

 *    [x] améliorer le script qui pour l'instant est extêmement basique et ne permet aucune option
 *   [x] automatiser l'execution du script à chaque commit
 *   [ ] publier les résultats sur le wiki et sortir une version pdf ou html
 *   [ ] ajouter des scripts pour trier les entreprises par mots-clés
 *   [ ] ajouter d'autres entreprises
    
## n'hésitez pas à contribuer 

La liste fournie ici n'est pas exaustive et peut être facilement agrandie et/ou améliorée.
Une autre liste regroupant de nombreuses entreprises militantes peux se trouver sur le site http://www.cnll.fr/cnll/membres/

Merci beaucoup
