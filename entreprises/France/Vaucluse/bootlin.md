# bootlin

site : https://bootlin.com/


## Brève Description

Bootlin (anciennement Free Electrons) est une société d’ingénierie spécialisée en Linux embarqué 
et plus généralement en logiciel libre et open-source pour systèmes embarqués.

### Détails

la société Bootlin ( anciennement free electron) possède une expertise en linux embarqué, elle 
contribue largement au 
noyau linux et au projet de cross compilation buildroot. Elle propose des service d'integration de plate forme sous linux et de formation.
L'entreprise possède actuellement trois sites distincts : Orange, Lyon et Toulouse

## Stages et/ou entretiens passés : non/non

 
## Mots-clés

 * militant
 * embarqué
 * buildroot
 * hardware
 
## Contacts

        - tel : 04 84 25 80 96
        - mail :  jobs@bootlin.com
        - adresse : 136 avenue Maréchal Foch 84100 Orange
	
