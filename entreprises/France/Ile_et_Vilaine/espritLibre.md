# esprit-libre

site : http://esprit-libre-conseil.com/


## Brève Description

Basé près de Rennes, esprit libre propose toute une gamme de services pour répondre à vos besoins en informatique. Spécialisé dans le 
logiciel libre, nous savons prendre en compte l'existant et intégrer nos solutions dans l'infrastructure existante, que ce soit libre 
ou privateur.

### Détails

Nous proposons des prestations...

    de conseil pour identifier les solutions les plus adaptées à vos besoins et votre budget ;
    d'audit pour analyser le fonctionnement de votre informatique et trouver une résolution aux problématiques rencontrées ;
    de déploiement de solutions informatiques pour faire évoluer vos services numériques et améliorer ainsi votre productivité ;
    de maintenance (ou infogérance) de votre parc informatique : interventions curratives et préventives pour avoir un outil informatique toujours opérationnel ;
    de formation des utilisateurs et gestionnaires des solutions informatiques que nous déploierons pour vous

Nos domaines d'intervention sont plus particulièrement sur les thématiques suivantes:

    réseau : mise en place d'équipements réseau et/ou analyse d'un réseau en place
    outils collaboratifs : déployer un cloud d'entreprise, un intranet, une messagerie instantannée interne, des agendas et carnets d'adresses partagés...
    mails : déployer des solutions "tout en un" permettant le mail, les agendas et carnets d'adresses, la réservation de ressources...
    bureautique : passer vos outils bureautiques en libre sans perturber votre fonctionnement, ou même passer à Linux vos postes bureautique
    serveurs : qu'ils soient dédiés ou virtualisés, nous pouvons prendre en charge la maintenance et les évolutions de vos serveurs linux
    et bien d'autres choses encore, n'hésitez pas nous contacter


## stages et/ou entretiens passés : NON/NON

 
## Mots-clés

 * alliance-libre
 
## Contacts

	- tel : 07 82 05 32 56
	- mail : contact@esprit-libre-conseil.com
	- adresse : 35250 Saint Germain sur Ille
	
