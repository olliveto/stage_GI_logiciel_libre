# esprit-libre

site : http://esprit-libre-conseil.com/


## Brève Description

Basé près de Rennes, esprit libre propose toute une gamme de services pour répondre à vos besoins en informatique. Spécialisé dans le 
logiciel libre, nous savons prendre en compte l'existant et intégrer nos solutions dans l'infrastructure existante, que ce soit libre 
ou privateur.

### Détails


## stages et/ou entretiens passés : NON/NON

 
## Mots-clés

 * alliance-libre
 
## Contacts

	- tel : 07 82 05 32 56
	- mail : contact@esprit-libre-conseil.com
	- adresse : 35250 Saint Germain sur Ille
	
