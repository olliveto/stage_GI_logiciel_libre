# Linagora

site : https://linagora.com/


## Brève Description (1 ligne seulement)

Créée en 2000, LINAGORA se positionne aujourd’hui comme le leader français du logiciel libre.

### Détails

En pleine croissance et déjà présente sur quatre continents, nous avons le projet un peu fou de proposer un numérique libre, éthique 
et respectueux des utilisateurs à nos clients.

Nous les accompagnons vers leur indépendance technologique en leur permettant de s’affranchir des géants du numérique propriétaire.

Pour cela nous proposons et développons des logiciels innovants, fruit du travail de collaborateurs passionnés.

Afin de renforcer nos équipes nous recherchons plusieurs profils d’ingénieurs DevOps pour intégrer une équipe constituée de 
consultants technologiques dédiés à des projets structurants pour nos clients, sur des technologies Open Source.

## stages et/ou entretiens passés : NON/NON

 
## Mots-clés

 * SSLL
 * cloud
 
## Contacts

	- tel : +33 1 46 96 63 63
	- mail : info@linagora.com
	- adresse : 100 Terrasse Boieldieu 92042 Paris 
	
