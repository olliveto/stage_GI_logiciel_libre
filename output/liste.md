# listes d'entreprises
mer. oct. 24 22:21:53 CEST 2018
## Etranger
--------------------------
### Europe
--------------------------
#### Belgique
--------------------------
##### [MIND](../entreprises/Etranger/Europe/Belgique/mind.md)
**Description**

SSII dans le monde de l'embarqué, mind est une filliale de ESSENSIUM. Elle est spécialisée dans
le développement open source.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : open-source embarqué

**Adresse** : [ Gaston Geenslaan 9 B-3001 Leuven Belgium](https://openstreetmap.org/search?query=%20Gaston%20Geenslaan%209%20B-3001%20Leuven%20Belgium)
#### Republique_Tcheque
--------------------------
##### [Codasip](../entreprises/Etranger/Europe/Republique_Tcheque/Codasip.md)
**Description**

Based in the heart of Europe (Czech Republic), Codasip is a rapidly growing startup focused on redefining embedded processing for the IoT era through groundbreaking IP and automation technology.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : RISC_V embarqué

**Adresse** : [ Božetěchova 1/2 612 00 Brno](https://openstreetmap.org/search?query=%20Božetěchova%201/2%20612%2000%20Brno)
## France
--------------------------
### Bouches_du_Rhone
--------------------------
#### [Biblibre](../entreprises/France/Bouches_du_Rhone/biblibre.md)
**Description**

BibLibre a été fondé sur une idée : aider les bibliothèques, de tout type, à déployer des logiciels sous licence libre.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : bibliothèque militant

**Adresse** : [ 108 rue Breteuil 13006 MARSEILLE](https://openstreetmap.org/search?query=%20108%20rue%20Breteuil%2013006%20MARSEILLE)
### Ile_de_France
--------------------------
#### Haut_de_Seine
--------------------------
##### [Linagora](../entreprises/France/Ile_de_France/Haut_de_Seine/Linagora.md)
**Description**

Créée en 2000, LINAGORA se positionne aujourd’hui comme le leader français du logiciel libre.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : SSLL cloud

**Adresse** : [ 100 Terrasse Boieldieu 92042 Paris](https://openstreetmap.org/search?query=%20100%20Terrasse%20Boieldieu%2092042%20Paris)
### Ile_et_Vilaine
--------------------------
#### [esprit-libre](../entreprises/France/Ile_et_Vilaine/espritLibre.md)
**Description**

Basé près de Rennes, esprit libre propose toute une gamme de services pour répondre à vos besoins en informatique. Spécialisé dans le
logiciel libre, nous savons prendre en compte l'existant et intégrer nos solutions dans l'infrastructure existante, que ce soit libre
ou privateur.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : alliance-libre

**Adresse** : [ 35250 Saint Germain sur Ille](https://openstreetmap.org/search?query=%2035250%20Saint%20Germain%20sur%20Ille)
#### [savoir-faire linux](../entreprises/France/Ile_et_Vilaine/savoir-faireLinux.md)
**Description**

Savoir-faire Linux est expert en technologies libres et open source au Canada et en Europe. Fondée sur l’économie du savoir, l’entreprise est spécialisée en technologies de l’information, objets connectés et ingénierie logicielle.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : militant embarqué web progiciel IA

**Adresse** : [ 14 rue Dupont des Loges 35000 Rennes](https://openstreetmap.org/search?query=%2014%20rue%20Dupont%20des%20Loges%2035000%20Rennes)
### Loire_Atlantique
--------------------------
#### [2I2L](../entreprises/France/Loire_Atlantique/2i2l.md)
**Description**

La société 2i2L, pour Informatique Internet et Logiciel Libre, est une société de services en informatique, spécialisée dans la formation et l'accompagnement. Nous travaillons dans un objectif humain de transmissions de savoirs et de mise à niveau des compétences.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : alliance-libre Bureautique infographie

**Adresse** : [ 12 avenue Jules Verne 44230 Saint Sébastien sur Loire](https://openstreetmap.org/search?query=%2012%20avenue%20Jules%20Verne%2044230%20Saint%20Sébastien%20sur%20Loire)
#### [CodeLutin](../entreprises/France/Loire_Atlantique/CodeLutin.md)
**Description**

Petite SSLL militante de 15 salariés. Spécialisée dans les technologies Java et web (html/CSS/javascript).

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [X] stage(s) effectué(s) : [X] 

 **Mots-clés** : html/css/javascript java militant

**Adresse** : [ 12 avenue Jules Verne 44230 Saint-Sebastien-sur-Loire](https://openstreetmap.org/search?query=%2012%20avenue%20Jules%20Verne%2044230%20Saint-Sebastien-sur-Loire)
#### [esprit-libre](../entreprises/France/Loire_Atlantique/espritLibre.md)
**Description**

Basé près de Rennes, esprit libre propose toute une gamme de services pour répondre à vos besoins en informatique. Spécialisé dans le
logiciel libre, nous savons prendre en compte l'existant et intégrer nos solutions dans l'infrastructure existante, que ce soit libre
ou privateur.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : alliance-libre

**Adresse** : [ 35250 Saint Germain sur Ille](https://openstreetmap.org/search?query=%2035250%20Saint%20Germain%20sur%20Ille)
#### [Pigolabs](../entreprises/France/Loire_Atlantique/Pigolabs.md)
**Description**

PigoLabs est une entreprise specialisée dans les services
autour de Piwigo, comme Piwigo.com ou des services dédiés :
support spécifique, développement de plugins, création de
thème, installation et mise à jour.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : piwigo alliance-libre

**Adresse** : [ Nantes](https://openstreetmap.org/search?query=%20Nantes)
### Vaucluse
--------------------------
#### [bootlin](../entreprises/France/Vaucluse/bootlin.md)
**Description**

Bootlin (anciennement Free Electrons) est une société d’ingénierie spécialisée en Linux embarqué
et plus généralement en logiciel libre et open-source pour systèmes embarqués.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : militant embarqué buildroot hardware

**Adresse** : [ 136 avenue Maréchal Foch 84100 Orange](https://openstreetmap.org/search?query=%20136%20avenue%20Maréchal%20Foch%2084100%20Orange)
